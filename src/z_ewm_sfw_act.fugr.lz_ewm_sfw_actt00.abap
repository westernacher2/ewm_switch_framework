*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 08.05.2020 at 16:44:21
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZEWM_V_SFW_ACT..................................*
TABLES: ZEWM_V_SFW_ACT, *ZEWM_V_SFW_ACT. "view work areas
CONTROLS: TCTRL_ZEWM_V_SFW_ACT
TYPE TABLEVIEW USING SCREEN '0001'.
DATA: BEGIN OF STATUS_ZEWM_V_SFW_ACT. "state vector
          INCLUDE STRUCTURE VIMSTATUS.
DATA: END OF STATUS_ZEWM_V_SFW_ACT.
* Table for entries selected to show on screen
DATA: BEGIN OF ZEWM_V_SFW_ACT_EXTRACT OCCURS 0010.
INCLUDE STRUCTURE ZEWM_V_SFW_ACT.
          INCLUDE STRUCTURE VIMFLAGTAB.
DATA: END OF ZEWM_V_SFW_ACT_EXTRACT.
* Table for all entries loaded from database
DATA: BEGIN OF ZEWM_V_SFW_ACT_TOTAL OCCURS 0010.
INCLUDE STRUCTURE ZEWM_V_SFW_ACT.
          INCLUDE STRUCTURE VIMFLAGTAB.
DATA: END OF ZEWM_V_SFW_ACT_TOTAL.

*.........table declarations:.................................*
TABLES: ZEWM_SFW_ACT                   .
