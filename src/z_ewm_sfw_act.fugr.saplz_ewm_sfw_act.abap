* regenerated at 08.05.2020 16:44:21
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZ_EWM_SFW_ACTTOP.                 " Global Declarations
  INCLUDE LZ_EWM_SFW_ACTUXX.                 " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZ_EWM_SFW_ACTF...                 " Subroutines
* INCLUDE LZ_EWM_SFW_ACTO...                 " PBO-Modules
* INCLUDE LZ_EWM_SFW_ACTI...                 " PAI-Modules
* INCLUDE LZ_EWM_SFW_ACTE...                 " Events
* INCLUDE LZ_EWM_SFW_ACTP...                 " Local class implement.
* INCLUDE LZ_EWM_SFW_ACTT99.                 " ABAP Unit tests
  INCLUDE LZ_EWM_SFW_ACTF00                       . " subprograms
  INCLUDE LZ_EWM_SFW_ACTI00                       . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
