*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 08.05.2020 at 17:10:35
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZEWM_V_SFW_CRT..................................*
TABLES: ZEWM_V_SFW_CRT, *ZEWM_V_SFW_CRT. "view work areas
CONTROLS: TCTRL_ZEWM_V_SFW_CRT
TYPE TABLEVIEW USING SCREEN '0002'.
DATA: BEGIN OF STATUS_ZEWM_V_SFW_CRT. "state vector
          INCLUDE STRUCTURE VIMSTATUS.
DATA: END OF STATUS_ZEWM_V_SFW_CRT.
* Table for entries selected to show on screen
DATA: BEGIN OF ZEWM_V_SFW_CRT_EXTRACT OCCURS 0010.
INCLUDE STRUCTURE ZEWM_V_SFW_CRT.
          INCLUDE STRUCTURE VIMFLAGTAB.
DATA: END OF ZEWM_V_SFW_CRT_EXTRACT.
* Table for all entries loaded from database
DATA: BEGIN OF ZEWM_V_SFW_CRT_TOTAL OCCURS 0010.
INCLUDE STRUCTURE ZEWM_V_SFW_CRT.
          INCLUDE STRUCTURE VIMFLAGTAB.
DATA: END OF ZEWM_V_SFW_CRT_TOTAL.

*.........table declarations:.................................*
TABLES: ZEWM_SFW_CRIT                  .
