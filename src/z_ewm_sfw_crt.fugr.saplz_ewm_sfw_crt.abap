* regenerated at 08.05.2020 16:43:23
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZ_EWM_SFW_CRTTOP.                 " Global Declarations
  INCLUDE LZ_EWM_SFW_CRTUXX.                 " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZ_EWM_SFW_CRTF...                 " Subroutines
* INCLUDE LZ_EWM_SFW_CRTO...                 " PBO-Modules
* INCLUDE LZ_EWM_SFW_CRTI...                 " PAI-Modules
* INCLUDE LZ_EWM_SFW_CRTE...                 " Events
* INCLUDE LZ_EWM_SFW_CRTP...                 " Local class implement.
* INCLUDE LZ_EWM_SFW_CRTT99.                 " ABAP Unit tests
  INCLUDE LZ_EWM_SFW_CRTF00                       . " subprograms
  INCLUDE LZ_EWM_SFW_CRTI00                       . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
